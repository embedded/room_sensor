#include "xtimer.h"

#include "bmx280_params.h"
#include "bmx280.h"

#include "bh1750fvi_params.h"
#include "bh1750fvi.h"

#include "il3829.h"

/* Measurement sampling interval */
static const uint32_t measurement_interval = 30 * US_PER_SEC;

/* BME280 configuration */
static const bmx280_params_t bme280_params = {
    .i2c_dev = BMX280_PARAM_I2C_DEV,
    .i2c_addr = BMX280_PARAM_I2C_ADDR,
    .t_sb = BMX280_SB_62_5,
    .filter = BMX280_FILTER_OFF,
    .spi3w_en = 0,
    .run_mode = BMX280_MODE_FORCED,
    .temp_oversample = BMX280_OSRS_X2,
    .press_oversample = BMX280_OSRS_X16,
    .humid_oversample = BMX280_OSRS_X16,
};
