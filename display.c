#include "display.h"

#include "fmt.h"
#include "il3829.h"
#include "il3829_pictures.h"

#include "hack_40.h"
#define FONT   font_hack_40
#define FONT_W font_hack_40_width
#define FONT_H font_hack_40_height
#define FONT_S font_hack_40_char_size

void display_glyph(il3829_t *dev, uint8_t i, uint8_t x, uint16_t y)
{
    il3829_set_area(dev, x, x + FONT_W, y, y + FONT_H);
    il3829_write_buffer(dev, FONT[i], FONT_S);
}

void display_letter(il3829_t *dev, char c, uint8_t x, uint16_t y)
{
    uint8_t i = 0x7F & (uint8_t)(c - 0x20);
    display_glyph(dev, i, x, y);
}

void display_string(il3829_t *dev, const char *s, uint8_t x, uint16_t y)
{
    size_t len = fmt_strnlen(s, 100);
    for(size_t i = 0; i < len; i++) {
        display_letter(dev, s[i], x + (i*FONT_W), y);
    }
}

void display_number(il3829_t *dev, int32_t v, uint8_t len, uint8_t x, uint16_t y)
{
    char buf[sizeof(int32_t)*1 + 1] = {0};
    size_t l = fmt_s32_dfp(buf, v, -1);
    l = fmt_lpad(buf, l, len, ' ');
    display_string(dev, buf, x, y);
}

void display_temp_hum(il3829_t *dev, int32_t temp, int32_t hum)
{
    display_number(dev, temp, 4, 0, 80);
    display_string(dev, "\x7F" "C", 136, 80);
    display_number(dev, hum, 4, 0, 80+FONT_H);
    display_string(dev, "%H", 136, 80+FONT_H);
}

void display_temp_hum_full(il3829_t *dev, int32_t temp, int32_t hum)
{
    il3829_clear(dev);
    il3829_set_area(dev, 32, 32+128, 0, sizeof(riot_logo_128)/(128/8));
    il3829_write_buffer(dev, (uint8_t *)riot_logo_128, sizeof riot_logo_128);
    display_temp_hum(dev, temp, hum);
}
