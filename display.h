#pragma once

#include "il3829.h"

void display_glyph(il3829_t *dev, uint8_t i, uint8_t x, uint16_t y);
void display_letter(il3829_t *dev, char c, uint8_t x, uint16_t y);
void display_string(il3829_t *dev, const char *s, uint8_t x, uint16_t y);
void display_number(il3829_t *dev, int32_t v, uint8_t len, uint8_t x, uint16_t y);
void display_temp_hum(il3829_t *dev, int32_t temp, int32_t hum);
void display_temp_hum_full(il3829_t *dev, int32_t temp, int32_t hum);
