#include <stdio.h>
#include "xtimer.h"

#include "bmx280.h"
#include "bh1750fvi.h"
#include "il3829.h"
#include "il3829_params.h"

#include "config.h"
#include "measure.h"
#include "display.h"

int main(void)
{
    bmx280_t sensor;
    bh1750fvi_t light;
    il3829_t dev;

    /* Initialise BME280 */
    int init = bmx280_init(&sensor, &bme280_params);
    if (init != BMX280_OK) {
        printf("BME280 INIT FAILED: %i\n", init);
        return 1;
    }

    /* Initialise BH1750 */
    init = bh1750fvi_init(&light, (bh1750fvi_params_t*)bh1750fvi_params);
    if (init != 0) {
        printf("BH1740FVI INIT FAILED: %i\n", init);
        return 1;
    }

    /* Initialise display */
    init = il3829_init(&dev, &il3829_params[0],
                       200, 200, IL3829_Y_INC_X_INC);
    if (init != 0) {
        printf("IL3829 INIT FAILED: %i\n", init);
        return 1;
    }

    /* Run measurement loop */
    uint8_t refresh_count = 0;
    int32_t t_prev = -1000;
    int32_t h_prev = -1000;
    xtimer_ticks32_t last_wakeup = xtimer_now();
    while (1)
    {
        measurement_t m = measure(&sensor, &light);
        print_raw_values(&m);
        xtimer_usleep(100 * US_PER_MS);

        /* Convert to single digit decimals */
        int32_t t = (int32_t)m.t/10;
        int32_t h = (int32_t)m.h/10;

        if (t != t_prev || h != h_prev) {
            if (refresh_count == 0) {
                printf("FULL UPDATE (%i): %li -> %li, %li -> %li\n", refresh_count, t_prev, t, h_prev, h);
                il3829_init_full(&dev);
                il3829_activate(&dev);
                display_temp_hum_full(&dev, t, h);
                il3829_update_full(&dev);
                il3829_clear(&dev); /* Clear second buffer */
                il3829_deactivate(&dev);
            } else {
                printf("PART UPDATE (%i): %li -> %li, %li -> %li\n", refresh_count, t_prev, t, h_prev, h);
                il3829_init_part(&dev);
                il3829_activate(&dev);
                display_temp_hum_full(&dev, t, h);
                il3829_update_part(&dev);
                il3829_deactivate(&dev);
            }

            refresh_count++;
            t_prev = t;
            h_prev = h;
        } else {
            printf("NO UPDATE\n");
        }

        xtimer_periodic_wakeup(&last_wakeup, measurement_interval);
    }

    return 0;
}
