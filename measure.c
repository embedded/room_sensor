#include "measure.h"

measurement_t measure(bmx280_t *s, bh1750fvi_t *l)
{
    measurement_t m;
    m.t = bmx280_read_temperature(s);
    m.p = bmx280_read_pressure(s);
    m.h = bme280_read_humidity(s);
    m.l = bh1750fvi_sample(l);
    return m;
}
void print_values(measurement_t *m)
{
    int16_t t_v;
    uint32_t p_v;
    uint16_t h_v;
    uint8_t t_d, p_d, h_d;

    t_v = m->t / 100;
    t_d = m->t % 100;
    p_v = m->p / 100;
    p_d = m->p % 100;
    h_v = m->h / 100;
    h_d = m->h % 100;

    printf("%d.%02u °C; %lu.%02u mbar; %d.%02u %%H, %d lx\n",
           t_v, t_d, p_v, p_d, h_v, h_d, m->l);
}

void print_raw_values(measurement_t *m)
{
    printf("%d,%lu,%d,%d\n", m->t, m->p, m->h, m->l);
}

int is_valid(measurement_t *m)
{
    return (m->t != -32768) && (m->l != 21333);
}
