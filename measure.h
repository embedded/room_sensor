#include <stdio.h>
#include "bmx280.h"
#include "bh1750fvi.h"

typedef struct Measurement
{
    int16_t t;
    uint32_t p;
    uint16_t h;
    uint16_t l;
} measurement_t;

measurement_t measure(bmx280_t *s, bh1750fvi_t *l);
void print_values(measurement_t *m);
void print_raw_values(measurement_t *m);
int is_valid(measurement_t *m);
