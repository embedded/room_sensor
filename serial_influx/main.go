package main

import (
	"bufio"
	"flag"
	"io"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	"git.snt.utwente.nl/dingen/cloudburst/influxdb"
	"github.com/tarm/serial"
)

type LastPoint struct {
	sync.RWMutex
	*influxdb.EnvironmentalPoint
}

var (
	lastPoint LastPoint
	hostName  string
	db        *influxdb.Client
	debug     bool
)

func reader(s io.Reader) {
	r := bufio.NewReader(s)
	for {
		l, err := r.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		if debug {
			log.Printf("Serial: %s", l)
		}

		strVals := strings.Split(strings.TrimSpace(l), ",")
		if len(strVals) != 4 {
			continue
		}

		vals := make([]int, len(strVals))
		for i, v := range strVals {
			vals[i], err = strconv.Atoi(v)
			if err != nil {
				break
			}
		}

		if err != nil || vals[0] == -32768 {
			log.Printf("Discarding invalid measurement: %v", vals)
			continue
		}

		m := influxdb.NewEnvironmentalPoint(
			hostName,
			time.Now(),
			float64(vals[0])/100,
			float64(vals[1])/100,
			float64(vals[2])/100,
			float64(vals[3]),
		)

		if debug {
			log.Printf("Measurement: %+v", m)
		}

		err = db.WritePoints(m)
		if err != nil {
			log.Fatalf("Error writing to InfluxDB: %s", err)
		}
	}
}

func main() {
	var dev string
	flag.BoolVar(&debug, "debug", false, "Enable debug output")
	flag.StringVar(&hostName, "host", "", "Probe host name")
	flag.StringVar(&dev, "dev", "/dev/ttyACM0", "Serial device")
	flag.Parse()

	// Open serial interface
	port := &serial.Config{Name: dev, Baud: 115200}
	s, err := serial.OpenPort(port)
	if err != nil {
		log.Fatal(err)
	}

	// Open influxdb
	db, err = influxdb.NewClient(&influxdb.ClientConfig{
		Host:     "https://db.slxh.eu:8086",
		Database: "sensors",
		Username: "",
		Password: "",
	})

	reader(s)
}
